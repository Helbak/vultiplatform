export const model = new Model();

class Model {
    constructor() {
        this.stringPaint = '<div class="paint-module" id="paintModule" >\n' +
            '   <div class="paint-module__hide">\n' +
            '    <div class="paint-module__palette" id="paintModulePalette" style="visibility:hidden">\n' +
            '     <div class="paint-module__palette__buttons">\n' +
            '      <div class="paint-module__palette__buttons__color #000000" id="rgb(0,0,0)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #7F7F7F" id="rgb(127,127,127)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #880016" id="rgb(136,0,22)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #ED1C22" id="rgb(237,28,34)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #FF7F26" id="rgb(255,127,38)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #FEF200" id="rgb(254,242,0)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #23B14D" id="rgb(35,177,77)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #00A3E8" id="rgb(0,163,232)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #3F47CC" id="rgb(63,71,204)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #A349A3" id="rgb(163,73,163)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #FFFFFF" id="rgb(255,255,255)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #C3C3C3" id="rgb(195,195,195)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #B97A57" id="rgb(185,122,87)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #FEAEC9" id="rgb(254,174,201)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #FFC90D" id="rgb(255,201,13)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #EFE3AF" id="rgb(239,227,175)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #B5E51D" id="rgb(181,229,29)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #9AD9EA" id="rgb(154,217,234)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #7092BF" id="rgb(112,146,191)"></div>\n' +
            '      <div class="paint-module__palette__buttons__color #C7BFE6" id="rgb(199,191,230)"></div>\n' +
            '     </div>\n' +
            '    </div>\n' +
            '   </div>\n' +
            '   <div class="paint-module__buttons-tools">\n' +
            ' <button class="paint-module__buttons-tools__button btn-palette" id="paletteOpen"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-horizontal" id="horizontLine"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-vertical" id="verticalLine"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-line" id="justLine"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-square" id="square"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-flow" id="flow"></button>\n' +
            ' <button class="paint-module__buttons-tools__button btn-delete" id="delete"></button>\n' +
            '</div>\n' +
            '   <canvas width="825" height="504" class="paint-module__area" id="paintArea"></canvas>\n' +
            '  </div>\n';
        this.stringIndev = '<div class="indev-module" id="indevModule">\n' +
            '   <div class="in-dev__tooltip" id="tooltip" title=""></div>\n' +
            '   <div class="indev-module__main" id="indev">\n' +
            '   </div>\n' +
            '  </div>';
        this.stringCRUD =
            "<div class=\"crud-module\" id=\"crudModule\">\n" +
            "   <div class=\"crud-module__main\">\n" +
            "    <div class=\"crud-module__main__table\" style=\"overflow-y:scroll;\">\n" +
            "     <table class=\"crud-module__main__table-table\">\n" +
            "      <thead>\n" +
            "      <tr>\n" +
            "       <th>ID</th>\n" +
            "       <th>Username</th>\n" +
            "       <th>Password</th>\n" +
            "       <th>Email</th>\n" +
            "       <th>Phone</th>\n" +
            "       <th>Name</th>\n" +
            "       <th>Surname</th>\n" +
            "       <th>Gender</th>\n" +
            "       <th>Rel. Status</th>\n" +
            "       <th>Prog. Lang.</th>\n" +
            "       <th>Work Exp.</th>\n" +
            "      </tr>\n" +
            "      </thead>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "       <td></td>\n" +
            "      </tr>\n" +
            "     </table>\n" +
            "    </div>\n" +
            "    <div class=\"crud-module__main__input\">\n" +
            "     <input class=\"input-crud\" id=\"inputCrud\" placeholder=\"Put information in JSON format\">\n" +
            "    </div>\n" +
            "    <div class=\"crud-module__main__buttons\">\n" +
            "     <button class=\"button-crud\" id=\"btnCreate\">CREATE</button>\n" +
            "     <button class=\"button-crud\" id=\"btnRead\">READ</button>\n" +
            "     <button class=\"button-crud\" id=\"btnUpdate\">UPDATE</button>\n" +
            "     <button class=\"button-crud\" id=\"btnDelete\">DELETE</button>\n" +
            "     <button class=\"button-crud\" id=\"btnClear\">CLEAR</button>\n" +
            "     <button class=\"button-crud\" id=\"btnTop\">ADD TOP</button>\n" +
            "     <button class=\"button-crud\" id=\"btnMid\">ADD MID</button>\n" +
            "     <button class=\"button-crud\" id=\"btnEnd\">ADD END</button>\n" +
            "     <button class=\"button-crud\" id=\"btnSaveDb\">SAVE DB</button>\n" +
            "     <button class=\"button-crud\" id=\"btnRestoreDb\">RESTORE DB</button>\n" +
            "    </div>\n" +
            "\n" +
            "   </div>\n" +
            "  </div>\n";
        this.stringMain =
            "<div class=\"main-module\" id=\"mainModule\" >\n" +
            "   <div class=\"main-module__paint main-modules-back\">\n" +
            "    <img class=\"main-module__paint__img main-module-img\" src=\"images/016-bucket@3x.png\">\n" +
            "    <div class=\"main-modules-back__text\">\n" +
            "     <p class=\"main-module__text\" id=\"mainTextPaint\">PAINT</p>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__descriptipon\">\n" +
            "     <a class=\"descriptipon\" id=\"mainDescriptionPaint\">Paint allows you to draw a picture using tools: line, square, circle and eraser.</a>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__paint__button\">\n" +
            "    <button class=\"button-widget\" id=\"btnPaint\"> Begin ></button>\n" +
            "    </div>\n" +
            "   </div>\n" +
            "   <div class=\"main-module__registration main-modules-back\">\n" +
            "    <img class=\"main-module__registration__img main-module-img\" src=\"images/user@3x.png\">\n" +
            "    <div class=\"main-modules-back__text\">\n" +
            "     <p class=\"main-module__text\" id=\"mainTextRegistration\">REGISTRATION</p>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__descriptipon\">\n" +
            "     <a class=\"descriptipon\" id=\"mainDescriptionRegistration\">Sign up in social network, to communicate with people who create products.</a>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__registration__button\">\n" +
            "     <button class=\"button-widget\" id=\"btnRegistration\"> Begin ></button>\n" +
            "    </div>\n" +
            "   </div>\n" +
            "   <div class=\"main-module__crud main-modules-back\">\n" +
            "    <img class=\"main-module__crud__img main-module-img\" src=\"images/crudimg.png\">\n" +
            "    <div class=\"main-modules-back__text\">\n" +
            "     <p class=\"main-module__text\" id=\"mainTextCrud\">CRUD</p>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__descriptipon\">\n" +
            "     <a class=\"descriptipon\" id=\"mainDescriptionCrud\">Copy, Read, Update, Delete your`s user information in database simply and fast.</a>\n" +
            "    </div>\n" +
            "    <div class=\"main-module__crud__button\">\n" +
            "    <button class=\"button-widget\" id=\"btnCrud\"> Begin ></button>\n" +
            "    </div>\n" +
            "   </div>\n" +
            "  </div>";
    };

    getStringPaint() {
        return this.stringPaint;
    };

    getStringMain() {
        return this.stringMain;
    };

    getStringCRUD() {
        return this.stringCRUD;
    };

    getStringIndev() {
        return this.stringIndev;
    };
};