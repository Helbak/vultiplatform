class LogicCD {
    constructor() {
        this.tbody = document.getElementById('tbody');
        this.td = document.getElementsByTagName('td');
        this.tr = document.getElementsByTagName('tr');
        this.input = document.getElementById('inputCrud');
        this.login = document.getElementById('loginUsername');
        this.pass = document.getElementById('loginPassword');
        this.contentpage = document.getElementById('crudModule');
        this.loginpage = document.getElementById('loginModule');
        this.massageError = document.getElementById('massageError');
        this.loginUsernameError = document.getElementById('loginUsernameError');
        this.loginPasswordError = document.getElementById('loginPasswordError');
        this.arrayPerson = [];
    }

    createNewTrInTableEnd(username, password, email, phone, name, surname, gender, relationship, languages, experience) {

        if(this.arrayPerson.length > 9999){
            return;
        }

        const row = document.createElement("TR");
        const td1 = document.createElement("TD");
        const td2 = document.createElement("TD");
        const td3 = document.createElement("TD");
        const td4 = document.createElement("TD");
        const td5 = document.createElement("TD");           //тут создаю елементы для пуша в таблицу
        const td6 = document.createElement("TD");
        const td7 = document.createElement("TD");
        const td8 = document.createElement("TD");
        const td9 = document.createElement("TD");
        const td10 = document.createElement("TD");
        const td11 = document.createElement("TD");

        td1.appendChild(document.createTextNode(''));
        td2.appendChild (document.createTextNode(username));
        td3.appendChild (document.createTextNode(password));
        td4.appendChild (document.createTextNode(email));
        td5.appendChild (document.createTextNode(phone));
        td6.appendChild (document.createTextNode(name));                      //тут добавляю в каждую ячейку своё значение
        td7.appendChild (document.createTextNode(surname));
        td8.appendChild (document.createTextNode(gender));
        td9.appendChild (document.createTextNode(relationship));
        td10.appendChild (document.createTextNode(languages));
        td11.appendChild (document.createTextNode(experience));

        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        row.appendChild(td4);
        row.appendChild(td5);
        row.appendChild(td6);                           //добавляю все ячейки в строку
        row.appendChild(td7);
        row.appendChild(td8);
        row.appendChild(td9);
        row.appendChild(td10);
        row.appendChild(td11);

        tbody.appendChild(row);                         //добавляю строку в таблицу

        this.createTableId();

        return '+';

    };

    createNewTrInTableStart(username, password, email, phone, name, surname, gender, relationship, languages, experience) {

        if(this.arrayPerson.length > 9999){
            return console.log('end');
        }

        const startTable = this.tbody.getElementsByTagName('tbody')[0];                 //тут обозначаю родителя для вставки елемента в таблицу
        const tr = document.getElementsByTagName('tr');
        const row = document.createElement("TR");
        const td1 = document.createElement("TD");
        const td2 = document.createElement("TD");
        const td3 = document.createElement("TD");
        const td4 = document.createElement("TD");
        const td5 = document.createElement("TD");
        const td6 = document.createElement("TD");
        const td7 = document.createElement("TD");
        const td8 = document.createElement("TD");
        const td9 = document.createElement("TD");
        const td10 = document.createElement("TD");
        const td11 = document.createElement("TD");

        td1.appendChild(document.createTextNode(''));
        td2.appendChild (document.createTextNode(username));
        td3.appendChild (document.createTextNode(password));
        td4.appendChild (document.createTextNode(email));
        td5.appendChild (document.createTextNode(phone));
        td6.appendChild (document.createTextNode(name));
        td7.appendChild (document.createTextNode(surname));
        td8.appendChild (document.createTextNode(gender));
        td9.appendChild (document.createTextNode(relationship));
        td10.appendChild (document.createTextNode(languages));
        td11.appendChild (document.createTextNode(experience));

        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        row.appendChild(td4);
        row.appendChild(td5);
        row.appendChild(td6);
        row.appendChild(td7);
        row.appendChild(td8);
        row.appendChild(td9);
        row.appendChild(td10);
        row.appendChild(td11);

        tbody.insertBefore(row, tr[1]);

        this.createTableId();

        return console.log('Записано');
    };

    createNewTrInTableMiddle(username, password, email, phone, name, surname, gender, relationship, languages, experience) {

        if(this.arrayPerson.length > 9999){
            return console.log('end');
        }

        const tr = document.getElementsByTagName('tr');
        const Lenght = Math.ceil((tr.length + 1)/2);
        const row = document.createElement("TR");
        const td1 = document.createElement("TD");
        const td2 = document.createElement("TD");
        const td3 = document.createElement("TD");
        const td4 = document.createElement("TD");
        const td5 = document.createElement("TD");
        const td6 = document.createElement("TD");
        const td7 = document.createElement("TD");
        const td8 = document.createElement("TD");
        const td9 = document.createElement("TD");
        const td10 = document.createElement("TD");
        const td11 = document.createElement("TD");

        td1.appendChild(document.createTextNode(''));
        td2.appendChild (document.createTextNode(username));
        td3.appendChild (document.createTextNode(password));
        td4.appendChild (document.createTextNode(email));
        td5.appendChild (document.createTextNode(phone));
        td6.appendChild (document.createTextNode(name));
        td7.appendChild (document.createTextNode(surname));
        td8.appendChild (document.createTextNode(gender));
        td9.appendChild (document.createTextNode(relationship));
        td10.appendChild (document.createTextNode(languages));
        td11.appendChild (document.createTextNode(experience));

        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        row.appendChild(td4);
        row.appendChild(td5);
        row.appendChild(td6);
        row.appendChild(td7);
        row.appendChild(td8);
        row.appendChild(td9);
        row.appendChild(td10);
        row.appendChild(td11);

        this.tbody.insertBefore(row, tr[Lenght]);            //добавляю в таблицу методом insertBefore

        this.createTableId();
    };

    createTableId() {

        for(let i = 0, j = 1; i < this.td.length; i+=11, j++) {
            this.td[i].innerHTML = j;
        }

    };

    deleteTR() {

        this.arrayPerson = [];
        console.log(this.arrayPerson);
        const lenghtTRarray = this.tr.length;
        for(let i = 1; i < lenghtTRarray ; i++){
            this.tr[lenghtTRarray - i].remove();
        }

    };

    newIdArray() {

        for(let i = 0; i < this.arrayPerson.length; i++){
            this.arrayPerson[i].Id = i + 1;
        }
        console.log(this.arrayPerson);

    };

    deleteTRr() {

        const lenghtTRarray = this.tr.length;
        for(let i = 1; i < lenghtTRarray ; i++){
            this.tr[lenghtTRarray - i].remove();
        }

    };

    //-----------Validation-------------------//

    checkUsernameLength(username) {

        if  (typeof username === 'number' || typeof username === 'object' || typeof username === 'array' || username == undefined || username.length < 1 || username.length > 40) {
            return false;
        } else {
            return true;
        }

    };

    checkUsernameValidation(username) {

        if(typeof username === 'number' || typeof username === 'object' || typeof username === 'array' || username == undefined){
            return false;
        }
        if (/^[a-zA-Z]+$/.test(username)) {
            return true
        } else {
            return false;
        }

    };

    checkPasswordOnlyLength(password) {

        if  (typeof password === 'number' || typeof password === 'object' || typeof password === 'array' || password == undefined || password.length <= 5 || password.length >= 31) {return false;}

        if (password.length >= 6 || password.length <= 30) {
            return true;
        }

    };

    checkPasswordValidation(password) {

        if(typeof password === 'number' || typeof password === 'object' || typeof password === 'array' || password == undefined){
            return false;
        }
        if (/(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[a-z])^[^а-яА-Я]+$/.test(password)) {
            return true;
        } else {
            return false;
        }

    };

    checkEmailValidation(email) {
        if(typeof email === 'number' || typeof email === 'object' || typeof email === 'array' || email == undefined){
            return false;
        }
        if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            return true;
        } else {
            return false;
        }
    };

    checkPhoneNumberValidation (phoneNumber) {

        if(typeof phoneNumber === 'number' || typeof phoneNumber === 'object' || typeof phoneNumber === 'array' || phoneNumber == undefined || phoneNumber.length <= 11 || phoneNumber.length > 13){
            return false;
        }

        if (/^\(?([+1]{2})\)?[-.() ]?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})$/.test(phoneNumber)) {
            return true;
        } else if (/^\(?([+]{1}[-.() ]?[3]{1}[-.() ]?[8]{1}[-.() ]?[-.() ]?[0]{1})\)?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})$/.test(phoneNumber)) {
            return true;
        } else if (/^\(?([+]{1}[-.() ]?[9]{1}[-.() ]?[7]{1}[-.() ]?[-.() ]?[2]{1})\)?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})[-.() ]?([0-9]{1})$/.test(phoneNumber)) {
            return true;
        } else {
            return false;
        }
    };

    checkNameValidation(name) {

        if(typeof name === 'number' || typeof name === 'object' || typeof name === 'array' || name == undefined || name.length < 2 || name.length > 40){
            return false;
        }

        if (/^[A-Z]{1}[a-zA-Z]+$/.test(name)) {
            return true;
        } else {
            return false;
        }

    };

    checkListGender(datalist) {

        if(typeof datalist === 'number' || typeof datalist === 'object' || typeof datalist === 'array' || datalist == undefined){
            return false;
        }
        if (datalist === 'male' || datalist === 'female' || datalist === '') {
            return true;
        } else if (datalist !== 'male' || datalist !== 'female' || datalist !== ''){
            return false;
        }

    };

    checkListRelation(relation) {

        if(typeof relation === 'number' || typeof relation === 'object' || typeof relation === 'array' || relation == undefined){
            return false;
        }
        if (relation === 'single' || relation === 'married' || relation === 'divorced' ||  relation === '') {
            return true;
        } else if (relation !== 'single' || relation !== 'married' || relation !== 'divorced' ||  relation !== ''){
            return false;
        }
    };

    checkListLanguages(language) {

        if(typeof language === 'number' || typeof language === 'object' || typeof language === 'array' || language == undefined){
            return false;
        }

        let arrayLanguageClient = language.split(', ');
        let num = 0;
        const languages = ['C', 'C++', 'C#', 'Java', 'JavaScript', 'Matlab', 'PHP', 'Ruby', 'Python', 'R'];
        for(let i = 0; i < arrayLanguageClient.length; i++){
            let str = arrayLanguageClient[i];
            for(let j = 0; j < languages.length; j++){
                if(str === languages[j]){
                    num++;
                }
            }
        }
        if(num === arrayLanguageClient.length){
            return true;
        } else {
            return false;
        }

    };

    checkListExperience(experience) {

        if(typeof experience === 'number' || typeof experience === 'object' || typeof experience === 'array' || experience == undefined){
            return false;
        }

        const experiences = ['0-1 years', '1-2 years', '2-3 years', '3-4 years', '4 and more years']
        for (let i = 0; i < experiences.length; i++) {
            if (experiences[i] === experience) {
                return true;
            }
        }
        return false;

    };

    checkSernameValidation(name) {

        if(name === ''){
            return true;
        }

        if(typeof name === 'number' || typeof name === 'object' || typeof name === 'array' || name == undefined || name.length < 2 || name.length > 40){
            return false;
        }

        if (/^[A-Z]{1}[a-zA-Z]+$/.test(name)) {
            return true;
        } else {
            return false;
        }

    };

    checkValidationJSON(obj){

        if(typeof obj === 'number' || typeof obj === 'string' || typeof obj === 'array' || obj == undefined){
            return false;
        }

        const username = obj.Username;
        const password = obj.Password;
        const email = obj.Email;
        const phone = obj.Phone;
        const name = obj.Name;
        const surname = obj.Surname;
        const gender = obj.Gender;
        const relationship = obj.Relantionship_status;
        const languages = obj.Programming_languages;
        const experience = obj.Work_experience;

        if(this.checkUsernameValidation(username) === true &&
            this.checkUsernameLength(username) === true &&
            this.checkPasswordOnlyLength(password) === true &&
            this.checkPasswordValidation(password) === true &&
            this.checkEmailValidation(email) === true &&
            this.checkPhoneNumberValidation(phone) === true &&
            this.checkNameValidation(name) === true &&
            this.checkListGender(gender) === true &&
            this.checkListRelation(relationship) === true &&
            this.checkListLanguages(languages) === true &&
            this.checkListExperience(experience) === true &&
            this.checkSernameValidation(surname) === true){
            return true;
        } else {
            return false;
        }
    };

    //-----------Methods for local table-----------------//

    create(str) {

        try {
            const objJSON = JSON.parse(str);

            if(this.checkValidationJSON(objJSON) === false){
                return;
            }

            this.createNewTrInTableEnd(objJSON.Username, objJSON.Password, objJSON.Email, objJSON.Phone, objJSON.Name, objJSON.Surname, objJSON.Gender, objJSON.Relantionship_status, objJSON.Programming_languages, objJSON.Work_experience);

            this.arrayPerson.push(objJSON);

            this.newIdArray();

        } catch (e) {
            alert('Строка JSON содержит ошибку');
        }

    };

    addEnd(str) {

        try {
            const objJSON = JSON.parse(str);

            if(this.checkValidationJSON(objJSON) === false){
                return;
            }

            this.createNewTrInTableEnd(objJSON.Username, objJSON.Password, objJSON.Email, objJSON.Phone, objJSON.Name, objJSON.Surname, objJSON.Gender, objJSON.Relantionship_status, objJSON.Programming_languages, objJSON.Work_experience);

            this.arrayPerson.push(objJSON);

            this.newIdArray();

        } catch (e) {
            alert('Строка JSON содержит ошибку');
        }
    };

    addStart(str) {

        try {
            const objJSON = JSON.parse(str);

            if(this.checkValidationJSON(objJSON) === false){
                return;
            }

            this.createNewTrInTableStart(objJSON.Username, objJSON.Password, objJSON.Email, objJSON.Phone, objJSON.Name, objJSON.Surname, objJSON.Gender, objJSON.Relantionship_status, objJSON.Programming_languages, objJSON.Work_experience);

            this.arrayPerson.unshift(objJSON);

            this.newIdArray();
        } catch (e) {
            alert('Строка JSON содержит ошибку');
        }

    };

    addMiddle(str) {

        try{

            const objJSON = JSON.parse(str);

            if(this.checkValidationJSON(objJSON) === false){
                return;
            }

            this.createNewTrInTableMiddle(objJSON.Username, objJSON.Password, objJSON.Email, objJSON.Phone, objJSON.Name, objJSON.Surname, objJSON.Gender, objJSON.Relantionship_status, objJSON.Programming_languages, objJSON.Work_experience);

            this.arrayPerson.splice(Math.ceil(this.arrayPerson.length/2), 0, objJSON);

            this.newIdArray();

        } catch (e) {
            alert('Строка JSON содержит ошибку');
        }

    };

    save(array) {

//    for(let i = 0; i < array.length; i++){
//            
//        const xhr = new XMLHttpRequest();
//            
//        xhr.open('POST', 'http://localhost:3000/add', false);
//        xhr.setRequestHeader("Content-Type", "application/json");
//        xhr.send(JSON.stringify(array[i]));
//        if (xhr.status != 200) {
//            console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
//        } else {
//            console.log("+");
//        };    
//    }

        for(let i = 0; i < array.length; i++){

            new Promise(function(succeed, fail) {
                const request = new XMLHttpRequest();
                request.open("POST", 'http://localhost:3000/add', true);
                request.setRequestHeader('Content-Type', 'application/json');
                request.addEventListener("load", function() {
                    if (request.status < 400){
                        succeed(request.responseText);
                    } else {
                        fail(new Error("Request failed: " + request.statusText));
                    }
                });
                request.addEventListener("error", function() {
                    fail(new Error("Network error"));
                });
                request.send(JSON.stringify(array[i]));
            });
        }
    };

    read(string) {

        const str = JSON.stringify(this.arrayPerson[+string - 1]);
        this.input.value = str;

        if(str === undefined){
            this.massageError.style.display = 'block';
            this.input.value = '';
        } else {
            this.massageError.style.display = 'none';
        }

    };

    loginCrud(str1, str2) {
        console.log("loginCrud(VPERED)!!!!!!!!!");
        if(str1 !== 'admin'){
            this.loginUsernameError.style.display = 'block';
        } else {
            this.loginUsernameError.style.display = 'none';
        }

        if(str2 !== 'admin'){
            this.loginPasswordError.style.display = 'block';
        } else {
            this.loginPasswordError.style.display = 'none';
        }

        if(str1 === 'admin' && str2 === 'admin'){
            this.contentpage.style.display = "flex";
            this.loginpage.style.display = "none";
        }
    };

    deleteStr(str) {

        const objJSON = JSON.parse(str);

        if(objJSON <= 0){
            return false;
        }

        if(+objJSON > this.arrayPerson.length){
            this.massageError.style.display = 'block';
            return;
        } else {
            this.massageError.style.display = 'none';
        }

        this.arrayPerson.splice(objJSON - 1, 1);
        this.tr[str].remove();
        this.createTableId();
        this.newIdArray();
        this.input.value = '';
    };

    update (str) {

        const objJSON = JSON.parse(str);
        console.log(this.arrayPerson.length);

        if(+objJSON.Id > this.arrayPerson.length || typeof objJSON !== 'object'){
            this.massageError.style.display = 'block';
            return;
        } else {
            this.massageError.style.display = 'none';
            this.deleteTRr();
        }

        this.arrayPerson.splice(objJSON.Id - 1, 1);
        this.arrayPerson.splice(objJSON.Id - 1, 0, objJSON);
        for(let i = 0; i < this.arrayPerson.length; i++){
            this.createNewTrInTableEnd(
                this.arrayPerson[i].Username,
                this.arrayPerson[i].Password,
                this.arrayPerson[i].Email,
                this.arrayPerson[i].Phone,
                this.arrayPerson[i].Name,
                this.arrayPerson[i].Surname,
                this.arrayPerson[i].Gender,
                this.arrayPerson[i].Relantionship_status,
                this.arrayPerson[i].Programming_languages,
                this.arrayPerson[i].Work_experience
            );
        }
        this.input.value = '';
        this.newIdArray();
    };
}

module.exports = LogicCD;
