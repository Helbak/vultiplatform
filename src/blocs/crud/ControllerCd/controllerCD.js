// import ModelCD from "../modelCd/modelCD.js";
// import LogicCD from "../logicCd/logicCD.js";
const ModelCD = require('../modelCd/modelCD');
const  LogicCD = require('../logicCd/logicCD');



class ControllerCD {

    constructor() {
        this.logic = new LogicCD();
        this.model = new ModelCD();
        console.log(this.model + " model in controller");
        console.log(this.logic + " logic in controller");
    }

     onBtnClear() {
         this.logic.create(this.input.value);
         if(this.logic.checkValidationJSON(JSON.parse(this.input.value)) === true){
             this.model.onMassege();
         } else {
             this.model.offMassege();
         }
     }

    init(){
        console.log('Запущен круд  init()');
        console.log('Обращаемся в МОДЕЛ   this.model.test ' + this.model.test);
        console.log(this);


        this.btnAddMiddle = document.getElementById('btnMid');
        this.btnAddStart = document.getElementById('btnTop');
        this.btnRestore = document.getElementById('btnRestoreDb');
        this.btnCreate = document.getElementById('btnCreate');
        this.btnUpdate = document.getElementById('btnUpdate');
        this.btnDelete = document.getElementById('btnDelete');
        this.btnAddEnd = document.getElementById('btnEnd');
        this.btnSave = document.getElementById('btnSaveDb');
        this.btnRead = document.getElementById('btnRead');
        this.btnClear = document.getElementById('btnClear');
        this.btnLog = document.getElementById('btnSignUpLogin');
        console.log(this.btnLog.id + " test btnLog.id");
        this.input = document.getElementById('inputCrud');
        this.crudModule = document.getElementById('crudModule');

            this.btnCreate.addEventListener('click', ()=>{
            this.logic.create(this.input.value);
            if(this.logic.checkValidationJSON(JSON.parse(this.input.value)) === true){
                this.model.onMassege();
            } else {
                this.model.offMassege();
            }
        }, false);  // Добавление в конец таблицы

        this.btnAddEnd.addEventListener('click', ()=>{
            this.logic.addEnd(this.input.value);

            if (!this.input.value) {
                return;
            }

            if(this.logic.checkValidationJSON(JSON.parse(this.input.value)) === true){
                this.model.onMassege();
            } else {
                this.model.offMassege();
            }
        }, false);   // Добавление в конец таблицы

        this.btnAddStart.addEventListener('click', ()=>{
            this.logic.addStart(this.input.value);
            if(this.logic.checkValidationJSON(JSON.parse(this.input.value)) === true){
                this.model.onMassege();
            } else {
                this.model.offMassege();
            }
        }, false);  // Добавление в начало таблицы

        this.btnAddMiddle.addEventListener('click', ()=>{
            this.logic.addMiddle(this.input.value);
            if(this.logic.checkValidationJSON(JSON.parse(this.input.value)) === true){
                this.model.onMassege();
            } else {
                this.model.offMassege();
            }
        }, false);  // Добавление в середину таблицы

        this.btnSave.addEventListener('click', ()=>{

            const xhr = new XMLHttpRequest();

            xhr.open('POST', 'http://localhost:3000/adddeleteTable', false);

            xhr.send();

            if (xhr.status != 200) {
                console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
            } else {
                console.log("+");
            };

        }, false);

        this.btnSave.addEventListener('click', ()=>{this.logic.save(this.logic.arrayPerson)}, false);  // Добавление в базу данных

        this.btnRead.addEventListener('click', ()=>{this.logic.read(this.input.value)}, false);  // По ID вывожу в инпут JSON строку

        this.btnDelete.addEventListener('click', ()=>{this.logic.deleteStr(this.input.value)}, false);  //  Удаляю из локальной базы данных

        this.btnUpdate.addEventListener('click', ()=>{this.logic.update(this.input.value)}, false);  //  Заменяю локально значения JSON строки

        this.btnLog.addEventListener('click', ()=>{console.log(this.logic + " " + this.logic.value); this.logic.loginCrud(this.logic.login.value, this.logic.pass.value);}, false);

        this.btnLog.addEventListener('click', function () {
                console.log(this.logic + " VZLETAJ!!!")
            // this.crudModule.display = "flex";
        }.bind(this), false);


        this.btnClear.addEventListener('click', ()=>{

            this.logic.deleteTR();
            this.logic.massageError.style.display = 'none';

        });

        this.btnRestore.addEventListener('click', ()=>{

            this.logic.deleteTR();

            const xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://localhost:3000/all', false);
            xhr.send();
            if (xhr.status != 200) {
                alert('Ошибка ' + xhr.status + ': ' + xhr.statusText);
            } else {
                const array = JSON.parse(xhr.response);
                for(let i = 0;  i < array.length; i++){
                    console.log(i);
                    this.logic.arrayPerson.push(array[i]);
                    let username = array[i].Username;
                    let password = array[i].Password;
                    let email = array[i].Email;
                    let phone = array[i].Phone;
                    let name = array[i].Name;
                    let surname = array[i].Surname;
                    let gender = array[i].Gender;
                    let relationship = array[i].Relantionship_status;
                    let languages = array[i].Programming_languages;
                    let experience = array[i].Work_experience;
                    this.logic.createNewTrInTableEnd(username, password, email, phone, name, surname, gender, relationship, languages, experience);
                }
            }

            this.logic.newIdArray();

        }, false);  // Вывожу на экран все значения БД

    }

    // close() {
    //     this.btnCreate && this.btnCreate.removeEventListener('click', this.onBtnClear);
    // }

}
//
// export default ControllerCD;
module.exports = ControllerCD;