export const controller = new Controller();

import { controllerPt } from '../blocs/paint/controllerPt/controllerPt.js';
import { model } from "../model/model";
import ControllerCD from "../blocs/crud/ControllerCd/controllerCD";
import ControllerIndev from "../blocs/indev/controllerIndev/controllerIndev"

class Controller {
    constructor() {
        this.controllerCD = new ControllerCD();
        this.controllerIndev = new ControllerIndev();
    };

    init() {
        this.controllerCD.init();
        console.log('включилась ф-ия controllerPt.init(); ');
        const btnPaint = document.getElementById('btnPaint');
        const btnRegistration = document.getElementById('btnRegistration');
        const btnCrud = document.getElementById('btnCrud');
        const mainModule = document.getElementById('mainModule');
        const crudModule = document.getElementById('crudModule');
        const btnMenu = document.getElementById('btnMenu');
        const loginModule = document.getElementById('loginModule');
        const btnSignUpLogin = document.getElementById('btnSignUpLogin');
        const paintModule = document.getElementById('paintModule');
        const btnContacts = document.getElementById('btnContacts');
        const btnForum = document.getElementById('btnForum');
        const indevModule = document.getElementById('indevModule');

        console.log(paintModule + "paint");

        btnPaint.addEventListener('click', () => {
            console.log('сработал btnPaint.addEventListener', this);
            this.showPaint();
        }, false);
        btnRegistration.addEventListener('click', () => {
            this.showRegistration();
        }, false);
        btnCrud.addEventListener('click', () => {
            console.log('сработал btnCrud.addEventListener', this);
            this.showCrud();
        }, false);

        btnMenu.addEventListener('click', ()=>{
            location.reload();
        })
    }

    showPaint() {
        console.log('зашёл в ф-ию  showPaint()');
        const containerBody = document.getElementById('containerBody');
        console.log('вывожу ид поля containerBody' + containerBody.id);
        containerBody.innerHTML = model.getStringPaint();
        this.turnOnPaint();

        // this.controllerCD.close();
    };

    turnOnPaint() {
        console.log('зашёл в ф-ию  turnOnPaint()');
        controllerPt.init();
        // this.controllerCD.close();

        //hi
    };

    showRegistration() {
        console.log('зашёл в ф-ию  showRegistration()');
        const containerBody = document.getElementById('containerBody');
        console.log('вывожу ид поля containerBody  =  ' + containerBody.id);
        containerBody.innerHTML = model.getStringIndev();
        this.turnOnRegistration();

        // this.controllerCD.close();
    };

    turnOnRegistration() {
        console.log('зашёл в ф-ию  turnOnRegistration()');
        this.controllerIndev.init(1);

        // this.controllerCD.close();
    }

    showCrud() {
        const mainModule = document.getElementById('mainModule');
        const loginModule = document.getElementById('loginModule');
        mainModule.style.display = 'none';
        loginModule.style.display = 'flex';
    };


}






//
// export const controller = new Controller();
// import { controllerPt } from '../blocs/paint/controllerPt/controllerPt.js';
// import { model } from "../model/model";
// import ControllerCD from "../blocs/crud/ControllerCd/controllerCD";
//
//
// class Controller {
//     constructor() {
//         this.controllerCD = new ControllerCD();
//     };
//
//     init() {
//         this.controllerCD.init();
//         console.log('включилась ф-ия controllerPt.init(); ');
//         const btnPaint = document.getElementById('btnPaint');
//         const btnRegistration = document.getElementById('btnRegistration');
//         const btnCrud = document.getElementById('btnCrud');
//         const mainModule = document.getElementById('mainModule');
//         const crudModule = document.getElementById('crudModule');
//         const btnMenu = document.getElementById('btnMenu');
//         const loginModule = document.getElementById('loginModule');
//         const btnSignUpLogin = document.getElementById('btnSignUpLogin');
//         const paintModule = document.getElementById('paintModule');
//         const btnContacts = document.getElementById('btnContacts');
//         const btnForum = document.getElementById('btnForum');
//
//         console.log('создал все кнопки');
//
//         btnPaint.addEventListener('click', () => {
//             console.log('сработал btnPaint.addEventListener', this);
//             this.showPaint();
//         }, false);
//         btnRegistration.addEventListener('click', () => {
//             console.log('сработал btnRegistration.addEventListener', this);
//             this.showRegistration();
//         }, false);
//         btnCrud.addEventListener('click', () => {
//             console.log('сработал btnCrud.addEventListener', this);
//             this.showCrud();
//         }, false);
//
//     }
//
//     showPaint() {
//         console.log('зашёл в ф-ию  showPaint()');
//         const containerBody = document.getElementById('containerBody');
//         console.log('вывожу ид поля containerBody' + containerBody.id);
//         containerBody.innerHTML = model.getStringPaint();
//         this.turnOnPaint();
//     };
//
//     turnOnPaint() {
//         console.log('зашёл в ф-ию  turnOnPaint()');
//         controllerPt.init();
//     };
//
//     showRegistration() {
//
//         console.log('зашёл в ф-ию  showRegistration()');
//         const containerBody = document.getElementById('containerBody');
//         console.log('вывожу ид поля containerBody' + containerBody.id);
//         containerBody.innerHTML = model.getStringRegistr();
//     };
//
//     // turnOnRegistration() {
//     //     console.log('зашёл в ф-ию  turnOnRegistration()');
//     //     // controllerIndev.init();
//     //
//     //     // this.controllerCD.close();
//     // }
//
//     showCrud() {
//         const mainModule = document.getElementById('mainModule');
//         const loginModule = document.getElementById('loginModule');
//         mainModule.style.display = 'none';
//         loginModule.style.display = 'flex';
//     };
// }



