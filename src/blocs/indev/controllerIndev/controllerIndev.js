import ModelIndev from "../modelIndev/modelIndev.js";
import LogicIndev from "../logicIndev/logicIndev.js";


class ControllerIndev {
    constructor() {
        this.logic = new LogicIndev();
        this.model = new ModelIndev();
    }

    init(a) {
        console.log('сработала ф-ия ControllerIndev.init()')
        this.tooltip = document.getElementById('tooltip');
        const indev = document.getElementById('indev');
        indev.innerHTML = this.model.getStep1();

        const username = document.getElementById("username");
        const password = document.getElementById("password");
        const confirm = document.getElementById("confirm");
        const email = document.getElementById("email");
        const phone = document.getElementById("phone");

        if (a === 2) {
            if (this.logic.checkUsernameLength(this.model.username) === true && this.logic.checkUsernameValidation(this.model.username) === true) {
                this.redDrawer("username", 'blue');
            }
            if (this.logic.checkUsernameLength(this.model.username) === false || this.logic.checkUsernameValidation(this.model.username) === false) {
                this.redDrawer("username", 'red');
                this.model.username = '';
            }
            if (this.logic.checkPasswordLength(this.model.password) !== false && this.logic.checkPasswordValidation(this.model.password) === true) {
                this.redDrawer("password", 'blue');
            }
            if (this.logic.checkPasswordLength(this.model.password) === false || this.logic.checkPasswordValidation(this.model.password) === false) {
                this.redDrawer("password", 'red');
                this.model.password = '';
                this.model.confirm = '';
            }
            if (this.logic.checkConfirm(this.model.password, this.model.confirm) === true) {
                this.redDrawer("confirm", 'blue');
            }
            if (this.logic.checkConfirm(this.model.password, this.model.confirm) === false || this.model.password === '') {
                this.redDrawer("confirm", 'red');
                this.model.confirm = '';
            }
            if (this.logic.checkEmailValidation(this.model.email) === true) {
                this.redDrawer("email", 'blue');
            }
            if (this.logic.checkEmailValidation(this.model.email) === false) {
                this.redDrawer("email", 'red');
                this.model.email = '';
            }
            if (this.logic.checkPhoneNumberValidation(this.model.phone) === true) {
                this.redDrawer("phone", 'blue');
            }
            if (this.logic.checkPhoneNumberValidation(this.model.phone) === false) {
                this.redDrawer("phone", 'red');
                this.model.phone = '';
            }
        }
        ;
        username.value = this.model.username;
        phone.value = this.model.phone;
        password.value = this.model.password;
        confirm.value = this.model.confirm;
        email.value = this.model.email;

        username.addEventListener('keyup', function () {
                if (this.logic.checkUsernameLength(username.value) === true && this.logic.checkUsernameValidation(username.value) === true) {
                    this.redDrawer("username", 'blue');
                    this.model.setUsername(username.value);
                    this.hideTip();
                }
                if (this.logic.checkUsernameLength(username.value) === false || this.logic.checkUsernameValidation(username.value) === false) {
                    this.redDrawer("username", 'red');
                    this.model.setUsername('');
                    this.showTip("username");
                }
            }.bind(this),
            false);


        password.addEventListener('keyup', function () {
                if (this.logic.checkConfirm(password.value, confirm.value) === true) {
                    this.redDrawer("confirm", 'blue');
                }
                if (this.logic.checkConfirm(password.value, confirm.value) === false) {
                    this.redDrawer("confirm", 'red');
                }
                if (this.logic.checkPasswordLength(password.value) !== false && this.logic.checkPasswordValidation(password.value) === true) {
                    this.redDrawer("password", 'blue');
                    this.model.setPassword(password.value);
                    this.hideTip();
                }
                if (this.logic.checkPasswordLength(password.value) === false || this.logic.checkPasswordValidation(password.value) === false) {
                    this.redDrawer("password", 'red');
                    this.model.setPassword('');
                    this.showTip("password");
                }
            }.bind(this),
            false);


        confirm.addEventListener('keyup', function () {
                if (this.logic.checkConfirm(password.value, confirm.value) === true) {
                    this.redDrawer("confirm", 'blue');
                    this.model.setConfirm(confirm.value);
                    this.hideTip();
                }
                if (this.logic.checkConfirm(password.value, confirm.value) === false) {
                    this.redDrawer("confirm", 'red');
                    this.model.setConfirm('');
                    this.showTip("confirm");
                }
            }.bind(this),
            false);


        email.addEventListener('keyup', function () {
                if (this.logic.checkEmailValidation(email.value) === true) {
                    this.redDrawer("email", 'blue');
                    this.model.setEmail(email.value);
                    this.hideTip();
                }
                if (this.logic.checkEmailValidation(email.value) === false) {
                    this.redDrawer("email", 'red');
                    this.model.setEmail('');
                    this.showTip("email");
                }
            }.bind(this),
            false);

        phone.addEventListener('keyup', function () {
                if (this.logic.checkPhoneNumberValidation(phone.value) === true) {
                    this.redDrawer("phone", 'blue');
                    this.model.setPhone(phone.value);
                    this.hideTip();
                }
                if (this.logic.checkPhoneNumberValidation(phone.value) === false) {
                    this.redDrawer("phone", 'red');
                    this.model.setPhone('');
                    this.showPhoneTip();
                }
            }.bind(this),
            false);

        const nextFromStep1 = document.getElementById("nextButton");
        nextFromStep1.addEventListener('click', function () {
                this.hideTip();
                this.functionStep2(0);
            }.bind(this),
            false);
    };
    functionStep2() {

        const indev = document.getElementById('indev');
        indev.innerHTML = this.model.getStep2();
        const name = document.getElementById('name');
        const surname = document.getElementById('surname');
        const gender = document.getElementById('gender');
        const relation = document.getElementById('relation');

        if (this.model.countStep2 > 1) {
            if (this.logic.checkNameValidation(this.model.name) === true) {
                this.redDrawer("name", "blue");
            }
            if (this.logic.checkNameValidation(this.model.name) === false) {
                this.redDrawer("name", "red");
                this.model.name = '';
            }
            if (this.logic.checkNameValidation(this.model.surname) === true) {
                this.redDrawer("surname", "blue");
            }
            if (this.logic.checkNameValidation(this.model.surname) === false) {
                // this.redDrawer("surname", "red");
                this.model.surname = '';
            }
            // if(this.model.gender===''){
            //     this.redDrawer("gender", "red");
            // }
            if (this.model.gender !== '') {
                this.redDrawer("gender", "blue");
            }
            // if(this.model.relation===''){
            //     this.redDrawer("relation", "red");
            // }
            if (this.model.relation !== '') {
                this.redDrawer("relation", "blue");
            }
        }
        ;


        name.value = this.model.name;
        surname.value = this.model.surname;
        gender.value = this.model.gender;
        relation.value = this.model.relation;
        this.model.setCountStep2(2);

        name.addEventListener('keyup', function () {
                if (this.logic.checkNameValidation(name.value) === true) {
                    this.model.setName(name.value);
                    this.model.name = name.value;
                    this.redDrawer("name", "blue");
                    this.hideTip();
                }
                if (this.logic.checkNameValidation(name.value) === false) {
                    this.redDrawer("name", "red");
                    this.model.name = '';
                    this.showTip("name");
                }
            }.bind(this),
            false);

        surname.addEventListener('keyup', function () {
                if (this.logic.checkNameValidation(surname.value) === true) {
                    this.model.setSurname(surname.value);
                    this.model.surname = surname.value;
                    this.redDrawer("surname", "blue");
                    this.hideTip();
                }
                if (this.logic.checkNameValidation(surname.value) === false) {
                    this.redDrawer("surname", "red");
                    this.model.surname = '';
                    this.showTip("surname");
                }
            }.bind(this),
            false);

        gender.addEventListener('change', function () {
                this.model.setGender(gender.value);
                this.redDrawer("gender", "blue");
            }.bind(this),
            false);
        gender.addEventListener('keyup', function () {
                if (this.logic.checkListGender(gender.value) === false) {
                    gender.value = '';
                    this.model.setGender(gender.selectedIndex);
                    this.redDrawer("gender", "black");
                }
            }.bind(this),
            false);
        relation.addEventListener('change', function () {
                this.model.setRelation(relation.value);
                this.redDrawer("relation", "blue");
            }.bind(this),
            false);
        relation.addEventListener('keyup', function () {
                if (this.logic.checkListRelation(relation.value) === false) {
                    relation.value = '';
                    this.redDrawer("relation", "black");
                    this.model.setRelation(relation.value);
                }
            }.bind(this),
            false);
        const backButtonFrom2 = document.getElementById("backButtonFrom2");
        backButtonFrom2.addEventListener('click', function () {
                this.init(2);
                this.hideTip();
            }.bind(this),
            false);
        const nextButtonFrom2 = document.getElementById('nextButtonFrom2');

        nextButtonFrom2.addEventListener('click', function () {
                this.functionStep3();
                this.hideTip();
            }.bind(this),
            false);

    };

    functionStep3 () {

        const indev = document.getElementById('indev');
        indev.innerHTML = this.model.getStep3();


        const progrLangLeft = document.getElementById('progrLangLeft');
        const progrLangRight = document.getElementById('progrLangRight');
        const experience = document.getElementById('experience');
        const checkboxRules = document.getElementById('checkboxRules');
        const backButtonFromStep3 = document.getElementById('backButtonFromStep3');
        const registerButton = document.getElementById('registerButton');


        registerButton.disabled = true;
        if (
            this.model.rulesBox === true
            &&
            this.logic.checkNameValidation(this.model.name) === true
            &&
            this.logic.checkUsernameValidation(this.model.username) === true
            &&
            this.logic.checkUsernameLength(this.model.username) === true
            &&
            this.logic.checkPasswordValidation(this.model.password) === true
            &&
            this.logic.checkPasswordOnlyLength(this.model.password) === true
            &&
            this.logic.checkConfirm(this.model.confirm, this.model.password) === true
            &&
            this.logic.checkEmailValidation(this.model.email) === true &&
            this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
            this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
            this.model.stringLanguages.length >= 1 &&
            this.logic.checkStringIsEmpty(this.model.experience) === true
        ) {
            registerButton.disabled = false;
        }
        ;


        if (this.model.countStep3 > 1) {

            if (this.model.stringLanguages.length < 1) {
                this.redDrawer("progrLangLeft", "red");
            }
            if (this.model.stringLanguages.length >= 1) {
                this.redDrawer("progrLangLeft", "blue");
            }
            if (this.model.experience === '') {
                this.redDrawer("experience", "red");
            }
            if (this.model.experience !== '') {
                this.redDrawer("experience", "blue");
            }
        }
        ;

        progrLangLeft.value = this.model.stringLanguages;
        experience.value = this.model.experience;
        checkboxRules.checked = this.model.rulesBox;
        this.model.setCountStep3(2);

        progrLangRight.addEventListener('change', function () {
                const arrayLanguages = this.model.hisLanguages;
                if (this.logic.checkExistElement(arrayLanguages, progrLangRight.value) === false) {
                    progrLangRight.value = '';
                    return;
                }
                if (progrLangRight.value === 'No one') {
                    this.model.setHisLanguages(progrLangRight.value);
                    progrLangLeft.value = this.model.stringLanguages;

                }
                if (progrLangRight.value !== 'No one') {
                    this.model.setHisLanguages(progrLangRight.value);
                    const stringLanguages = this.logic.arrayToString(arrayLanguages);
                    this.model.stringLanguages = stringLanguages;
                    progrLangLeft.value = stringLanguages;
                }
                progrLangRight.value = '';
                this.redDrawer("progrLangLeft", "blue");
                if (
                    this.model.rulesBox === true
                    &&
                    this.logic.checkNameValidation(this.model.name) === true
                    &&
                    this.logic.checkUsernameValidation(this.model.username) === true
                    &&
                    this.logic.checkUsernameLength(this.model.username) === true
                    &&
                    this.logic.checkPasswordValidation(this.model.password) === true
                    &&
                    this.logic.checkPasswordOnlyLength(this.model.password) === true
                    &&
                    this.logic.checkConfirm(this.model.confirm, this.model.password) === true
                    &&
                    this.logic.checkEmailValidation(this.model.email) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.model.stringLanguages.length >= 1 &&
                    this.logic.checkStringIsEmpty(this.model.experience) === true
                ) {
                    registerButton.disabled = false;

                }
                ;
            }.bind(this),
            false);

        experience.addEventListener('change', function () {
                this.model.setExperience(experience.value);
                this.redDrawer("experience", "blue");
                if (
                    this.model.rulesBox === true
                    &&
                    this.logic.checkNameValidation(this.model.name) === true
                    &&
                    this.logic.checkUsernameValidation(this.model.username) === true
                    &&
                    this.logic.checkUsernameLength(this.model.username) === true
                    &&
                    this.logic.checkPasswordValidation(this.model.password) === true
                    &&
                    this.logic.checkPasswordOnlyLength(this.model.password) === true
                    &&
                    this.logic.checkConfirm(this.model.confirm, this.model.password) === true
                    &&
                    this.logic.checkEmailValidation(this.model.email) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.model.stringLanguages.length >= 1 &&
                    this.logic.checkStringIsEmpty(this.model.experience) === true
                ) {

                    registerButton.disabled = false;

                }
                ;
            }.bind(this),
            false);
        experience.addEventListener('keyup', function () {
                if (this.logic.checkListExperience(experience.value, this.model.experiences) === false) {
                    experience.value = '';
                    this.redDrawer("experience", "black");
                    this.model.setExperience(experience.value);
                }
            }.bind(this),
            false);


        backButtonFromStep3.addEventListener('click', function () {
                this.functionStep2();
                this.hideTip();
            }.bind(this),
            false);

        checkboxRules.addEventListener('change', function () {
                if (this.model.setRulesBox(checkboxRules.checked) === true) {
                    this.hideTip();
                    if (
                        this.model.rulesBox === true
                        &&
                        this.logic.checkNameValidation(this.model.name) === true
                        &&
                        this.logic.checkUsernameValidation(this.model.username) === true
                        &&
                        this.logic.checkUsernameLength(this.model.username) === true
                        &&
                        this.logic.checkPasswordValidation(this.model.password) === true
                        &&
                        this.logic.checkPasswordOnlyLength(this.model.password) === true
                        &&
                        this.logic.checkConfirm(this.model.confirm, this.model.password) === true
                        &&
                        this.logic.checkEmailValidation(this.model.email) === true &&
                        this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                        this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                        this.model.stringLanguages.length >= 1 &&
                        this.logic.checkStringIsEmpty(this.model.experience) === true
                    ) {

                        registerButton.disabled = false;

                    }
                    ;
                } else {
                    this.showTip("checkboxRules");
                }
                this.model.setRulesBox(checkboxRules.checked);
                if (checkboxRules.checked === false) {
                    registerButton.disabled = true;
                }
            }.bind(this),
            false);

        const btnRules = document.getElementById('btnRules');
        btnRules.addEventListener('click', function () {
                this.functionRules();
            }.bind(this),
            false);


        registerButton.addEventListener('click', function () {
                if (
                    this.model.rulesBox === true
                    &&
                    this.logic.checkNameValidation(this.model.name) === true
                    &&
                    this.logic.checkUsernameValidation(this.model.username) === true
                    &&
                    this.logic.checkUsernameLength(this.model.username) === true
                    &&
                    this.logic.checkPasswordValidation(this.model.password) === true
                    &&
                    this.logic.checkPasswordOnlyLength(this.model.password) === true
                    &&
                    this.logic.checkConfirm(this.model.confirm, this.model.password) === true
                    &&
                    this.logic.checkEmailValidation(this.model.email) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.logic.checkPhoneNumberValidation(this.model.phone) === true &&
                    this.model.stringLanguages.length >= 1 &&
                    this.logic.checkStringIsEmpty(this.model.experience) === true
                ) {

                    const indev = document.getElementById('indev');
                    indev.innerHTML = this.model.getStep4();
                    console.log("тут высылать в базу " + this.model.name+" "+this.model.username+'  '+this.model.password+' '+this.model.email+' '+this.model.phone+"  "+this.model.experience);
                    this.sendPersonToDB();
                }
                ;
            }.bind(this),
            false);
    };
    sendPersonToDB (){
        const xhr = new XMLHttpRequest();

        const person = {
            Username:this.model.username,
            Password:this.model.password,
            Email:this.model.email,
            Phone:this.model.phone,
            Name:this.model.name,
            Surname:this.model.surname,
            Gender:this.model.gender,
            Relantionship_status:this.model.relation,
            Programming_languages:this.model.stringLanguages,
            Work_experience:this.model.experience
        };
        console.log(person);
        xhr.open('POST', 'http://localhost:3000/add', false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(person));
        if (xhr.status != 200) {
            console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
        } else {
            console.log("+");
        };
    }
    functionRules () {
        const indev = document.getElementById('indev');
        indev.innerHTML = this.model.getRules();
        const backButtonFromRules = document.getElementById('backButtonFromRules');
        backButtonFromRules.addEventListener('click', function () {
                this.functionStep3();
            }.bind(this),
            false);
    };
// Controller.prototype.functionResult = function () {
// window.open('successedRegistr.html');
// const indev = document.getElementById('indev');
// indev.innerHTML = this.model.getResult();
// const usernameResult = document.getElementById("usernameResult");
// const passwordResult = document.getElementById("passwordResult");
// const confirmResult = document.getElementById("confirmResult");
// const emailResult = document.getElementById("emailResult");
// const phoneResult = document.getElementById("phoneResult");
// const nameResult = document.getElementById("nameResult");
// const surnameResult = document.getElementById("surnameResult");
// const genderResult = document.getElementById("genderResult");
// const relationResult = document.getElementById("relationResult");
// const progrLangResult = document.getElementById("progrLangResult");
// const experienceResult = document.getElementById("experienceResult");
//
// usernameResult.value = "username: " + this.model.username;
// passwordResult.value = "password: " + this.model.password;
// confirmResult.value = "confirm: " + this.model.confirm;
// emailResult.value = "email: " + this.model.email;
// phoneResult.value = "phone: " + this.model.phone;
// nameResult.value = "name: " + this.model.name;
// genderResult.value = "gender: " + this.model.gender;
// surnameResult.value = "surname: " + this.model.surname;
// relationResult.value = "relation: " + this.model.relation;
// progrLangResult.value = "progrLang: " + this.model.progrLang;
// experienceResult.value = "experience: " + this.model.experience;

// const backButtonFromResult = document.getElementById("backButtonFromResult");
// backButtonFromResult.addEventListener('click', function () {
//         this.init();
//     }.bind(this),
//     false);
// };

    redDrawer (id, color) {

        let area = document.getElementById(id);
        if (color === 'black') {
            area.style = "border-bottom-color: rgba(9, 1, 1, 0.95);";
        }
        if (color === 'red') {
            // area.style = "color: rgba(254, 27, 46, 0.95)";
            area.style = "border-bottom-color: rgb(228, 17, 17)";

        }
        if (color === 'blue') {
            area.style = "border-bottom-color:  rgba(55, 68, 206)";
        }
    };

    showTip (id) {
        switch (id) {
            case ('username'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.usernameTipString;
                this.tooltip.style.top = '250px';
                this.tooltip.style.left = '460px';
                break;
            case ('password'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.passwordTipString;
                this.tooltip.style.top = '320px';
                this.tooltip.style.left = '460px';
                break;
            case('confirm'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.confirmTipString;
                this.tooltip.style.top = '385px';
                this.tooltip.style.left = '460px';
                break;
            case('email'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.emailTipString;
                this.tooltip.style.top = '450px';
                this.tooltip.style.left = '460px';
                break;
            case('name'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.nameTipString;
                this.tooltip.style.top = '250px';
                this.tooltip.style.left = '460px';
                break;
            case('surname'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.surnameTipString;
                this.tooltip.style.top = '320px';
                this.tooltip.style.left = '460px';
                break;
            case('checkboxRules'):
                this.tooltip.style.display = 'block';
                this.tooltip.innerHTML = this.model.checkBoxTipString;
                this.tooltip.style.top = '450px';
                this.tooltip.style.left = '528px';
                break;
            default :
                break;
        }
    };


    showPhoneTip () {
        if (phone.value[0] !== '+') {
            this.tooltip.style.display = 'block';
            this.tooltip.innerHTML = this.model.phoneTipString1;
            this.tooltip.style.top = '515px';
            this.tooltip.style.left = '460px';
            //подсказка про коды ниже
        } else if (phone.value[0] === '+' && phone.value !== '+1' && phone.value !== '+380' && phone.value !== '+972' && phone.value.length < 5) {
            this.tooltip.style.display = 'block';
            this.tooltip.innerHTML = this.model.phoneTipString2;
            this.tooltip.style.top = '515px';
            this.tooltip.style.left = '460px';
            ;
            //подсказка про длину цифр ниже
        } else if (phone.value.substring(0, 2) === '+1' || phone.value.substring(0, 4) === '+380' || phone.value.substring(0, 4) === '+972') {
            this.tooltip.style.display = 'block';
            this.tooltip.innerHTML = this.model.phoneTipString3;
            this.tooltip.style.top = '515px';
            this.tooltip.style.left = '460px';
            //подсказка про только цифры ниже
        }
        if (phone.value[0] === '+' && /[А-Яа-яA-Za-z  ]+$/.test(phone.value)) {
            this.tooltip.style.display = 'block';
            this.tooltip.innerHTML = this.model.phoneTipString4;
            this.tooltip.style.top = '515px';
            this.tooltip.style.left = '460px';
        }
    };


    hideTip () {
        return this.tooltip.style.display = 'none';
    };
}
export default ControllerIndev;